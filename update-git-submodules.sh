#!/bin/sh

git submodule update --init --recursive --remote
( cd Scripts && git checkout master ; )
( cd Text && git checkout master ; )
( cd Website && git checkout master ; )
git add Scripts/ Text/ Website/ && git commit -m "Update submodule refs"

