using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for BookmarksForm.
	/// </summary>
	public class BookmarksForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ToolBar toolbar;
		private System.Windows.Forms.ToolBarButton tlbNew;
		private System.Windows.Forms.ImageList imgToolbar;
		private System.Windows.Forms.ToolBarButton tlbSep1;
		private System.Windows.Forms.ToolBarButton tlbSep3;
		private System.Windows.Forms.ToolBarButton tlbBold;
		private System.Windows.Forms.ToolBarButton tlbItalic;
		private System.Windows.Forms.TextBox txtRef;
		private System.Windows.Forms.RichTextBox rtbDesc;
		private System.Windows.Forms.ImageList imgTV;
		public System.Windows.Forms.TreeView tvwbm;
		private System.ComponentModel.IContainer components;
		public Hashtable hashRef = new Hashtable(), hashText=new Hashtable();
		private System.Windows.Forms.ToolBarButton tlbDelete;
		private System.Windows.Forms.ToolBarButton tlbMoveUp;
		private System.Windows.Forms.ToolBarButton tlbInsert;
		private System.Windows.Forms.ToolBarButton tlbAfter;
		private System.Windows.Forms.Timer tmrDragOver;
		private System.Windows.Forms.ToolBarButton tlbMoveDown;
		public System.Windows.Forms.ToolBarButton tlbSearch;
		private System.Windows.Forms.ToolBarButton tlbSep2;
		private System.Windows.Forms.Button btnAddRef;

		private TreeNode dragOver=new TreeNode();
        private ContextMenuStrip menuBMEdit;
        private ToolStripMenuItem menuBMEditCut;
        private ToolStripMenuItem menuBMEditCopy;
        private ToolStripMenuItem menuBMEditPaste;
        private ContextMenuStrip menuBM;
        private ToolStripMenuItem menuBMinside;
        private ToolStripMenuItem menuBMbefore;
        private ToolStripMenuItem menuBMafter;
        private ToolStripMenuItem menuBMdelete;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem menuBMup;
        private ToolStripMenuItem menuBMdown;

		public BookmarksForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookmarksForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddRef = new System.Windows.Forms.Button();
            this.rtbDesc = new System.Windows.Forms.RichTextBox();
            this.txtRef = new System.Windows.Forms.TextBox();
            this.tvwbm = new System.Windows.Forms.TreeView();
            this.imgTV = new System.Windows.Forms.ImageList(this.components);
            this.toolbar = new System.Windows.Forms.ToolBar();
            this.tlbNew = new System.Windows.Forms.ToolBarButton();
            this.tlbInsert = new System.Windows.Forms.ToolBarButton();
            this.tlbAfter = new System.Windows.Forms.ToolBarButton();
            this.tlbDelete = new System.Windows.Forms.ToolBarButton();
            this.tlbSep1 = new System.Windows.Forms.ToolBarButton();
            this.tlbMoveUp = new System.Windows.Forms.ToolBarButton();
            this.tlbMoveDown = new System.Windows.Forms.ToolBarButton();
            this.tlbSep3 = new System.Windows.Forms.ToolBarButton();
            this.tlbSearch = new System.Windows.Forms.ToolBarButton();
            this.tlbSep2 = new System.Windows.Forms.ToolBarButton();
            this.tlbBold = new System.Windows.Forms.ToolBarButton();
            this.tlbItalic = new System.Windows.Forms.ToolBarButton();
            this.imgToolbar = new System.Windows.Forms.ImageList(this.components);
            this.tmrDragOver = new System.Windows.Forms.Timer(this.components);
            this.menuBMEdit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuBMEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBM = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuBMinside = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMbefore = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMafter = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMdelete = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMup = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBMdown = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1.SuspendLayout();
            this.menuBMEdit.SuspendLayout();
            this.menuBM.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAddRef);
            this.panel1.Controls.Add(this.rtbDesc);
            this.panel1.Controls.Add(this.txtRef);
            this.panel1.Controls.Add(this.tvwbm);
            this.panel1.Controls.Add(this.toolbar);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnAddRef
            // 
            resources.ApplyResources(this.btnAddRef, "btnAddRef");
            this.btnAddRef.Name = "btnAddRef";
            this.btnAddRef.Click += new System.EventHandler(this.btnAddRef_Click);
            // 
            // rtbDesc
            // 
            resources.ApplyResources(this.rtbDesc, "rtbDesc");
            this.rtbDesc.ContextMenuStrip = this.menuBMEdit;
            this.rtbDesc.Name = "rtbDesc";
            this.rtbDesc.TextChanged += new System.EventHandler(this.rtbDesc_TextChanged);
            // 
            // txtRef
            // 
            resources.ApplyResources(this.txtRef, "txtRef");
            this.txtRef.Name = "txtRef";
            this.txtRef.TextChanged += new System.EventHandler(this.txtRef_TextChanged);
            this.txtRef.DoubleClick += new System.EventHandler(this.txtRef_DoubleClick);
            // 
            // tvwbm
            // 
            this.tvwbm.AllowDrop = true;
            resources.ApplyResources(this.tvwbm, "tvwbm");
            this.tvwbm.ContextMenuStrip = this.menuBM;
            this.tvwbm.HideSelection = false;
            this.tvwbm.ImageList = this.imgTV;
            this.tvwbm.ItemHeight = 16;
            this.tvwbm.LabelEdit = true;
            this.tvwbm.Name = "tvwbm";
            this.tvwbm.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("tvwbm.Nodes")))});
            this.tvwbm.ShowRootLines = false;
            this.tvwbm.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvwbm_ItemDrag);
            this.tvwbm.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwbm_AfterSelect);
            this.tvwbm.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvwbm_DragDrop);
            this.tvwbm.DragEnter += new System.Windows.Forms.DragEventHandler(this.tvwbm_DragEnter);
            this.tvwbm.DragOver += new System.Windows.Forms.DragEventHandler(this.tvwbm_DragOver);
            // 
            // imgTV
            // 
            this.imgTV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgTV.ImageStream")));
            this.imgTV.TransparentColor = System.Drawing.Color.Transparent;
            this.imgTV.Images.SetKeyName(0, "");
            this.imgTV.Images.SetKeyName(1, "");
            this.imgTV.Images.SetKeyName(2, "");
            // 
            // toolbar
            // 
            resources.ApplyResources(this.toolbar, "toolbar");
            this.toolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tlbNew,
            this.tlbInsert,
            this.tlbAfter,
            this.tlbDelete,
            this.tlbSep1,
            this.tlbMoveUp,
            this.tlbMoveDown,
            this.tlbSep3,
            this.tlbSearch,
            this.tlbSep2,
            this.tlbBold,
            this.tlbItalic});
            this.toolbar.CausesValidation = false;
            this.toolbar.ImageList = this.imgToolbar;
            this.toolbar.Name = "toolbar";
            this.toolbar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolbar_ButtonClick);
            // 
            // tlbNew
            // 
            resources.ApplyResources(this.tlbNew, "tlbNew");
            this.tlbNew.Name = "tlbNew";
            // 
            // tlbInsert
            // 
            resources.ApplyResources(this.tlbInsert, "tlbInsert");
            this.tlbInsert.Name = "tlbInsert";
            // 
            // tlbAfter
            // 
            resources.ApplyResources(this.tlbAfter, "tlbAfter");
            this.tlbAfter.Name = "tlbAfter";
            // 
            // tlbDelete
            // 
            resources.ApplyResources(this.tlbDelete, "tlbDelete");
            this.tlbDelete.Name = "tlbDelete";
            // 
            // tlbSep1
            // 
            this.tlbSep1.Name = "tlbSep1";
            this.tlbSep1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tlbMoveUp
            // 
            resources.ApplyResources(this.tlbMoveUp, "tlbMoveUp");
            this.tlbMoveUp.Name = "tlbMoveUp";
            // 
            // tlbMoveDown
            // 
            resources.ApplyResources(this.tlbMoveDown, "tlbMoveDown");
            this.tlbMoveDown.Name = "tlbMoveDown";
            // 
            // tlbSep3
            // 
            this.tlbSep3.Name = "tlbSep3";
            this.tlbSep3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tlbSearch
            // 
            resources.ApplyResources(this.tlbSearch, "tlbSearch");
            this.tlbSearch.Name = "tlbSearch";
            this.tlbSearch.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            // 
            // tlbSep2
            // 
            this.tlbSep2.Name = "tlbSep2";
            this.tlbSep2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tlbBold
            // 
            resources.ApplyResources(this.tlbBold, "tlbBold");
            this.tlbBold.Name = "tlbBold";
            // 
            // tlbItalic
            // 
            resources.ApplyResources(this.tlbItalic, "tlbItalic");
            this.tlbItalic.Name = "tlbItalic";
            // 
            // imgToolbar
            // 
            this.imgToolbar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgToolbar.ImageStream")));
            this.imgToolbar.TransparentColor = System.Drawing.Color.Transparent;
            this.imgToolbar.Images.SetKeyName(0, "");
            this.imgToolbar.Images.SetKeyName(1, "");
            this.imgToolbar.Images.SetKeyName(2, "");
            this.imgToolbar.Images.SetKeyName(3, "");
            this.imgToolbar.Images.SetKeyName(4, "");
            this.imgToolbar.Images.SetKeyName(5, "");
            this.imgToolbar.Images.SetKeyName(6, "");
            this.imgToolbar.Images.SetKeyName(7, "");
            this.imgToolbar.Images.SetKeyName(8, "");
            // 
            // tmrDragOver
            // 
            this.tmrDragOver.Interval = 500;
            this.tmrDragOver.Tick += new System.EventHandler(this.tmrDragOver_Tick);
            // 
            // menuBMEdit
            // 
            this.menuBMEdit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBMEditCut,
            this.menuBMEditCopy,
            this.menuBMEditPaste});
            this.menuBMEdit.Name = "menuBMEdit";
            resources.ApplyResources(this.menuBMEdit, "menuBMEdit");
            // 
            // menuBMEditCut
            // 
            this.menuBMEditCut.Name = "menuBMEditCut";
            resources.ApplyResources(this.menuBMEditCut, "menuBMEditCut");
            this.menuBMEditCut.Click += new System.EventHandler(this.menuBMEditCut_Click);
            // 
            // menuBMEditCopy
            // 
            this.menuBMEditCopy.Name = "menuBMEditCopy";
            resources.ApplyResources(this.menuBMEditCopy, "menuBMEditCopy");
            this.menuBMEditCopy.Click += new System.EventHandler(this.menuBMEditCopy_Click);
            // 
            // menuBMEditPaste
            // 
            this.menuBMEditPaste.Name = "menuBMEditPaste";
            resources.ApplyResources(this.menuBMEditPaste, "menuBMEditPaste");
            this.menuBMEditPaste.Click += new System.EventHandler(this.menuBMEditPaste_Click);
            // 
            // menuBM
            // 
            this.menuBM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBMinside,
            this.menuBMbefore,
            this.menuBMafter,
            this.menuBMdelete,
            this.toolStripSeparator1,
            this.menuBMup,
            this.menuBMdown});
            this.menuBM.Name = "menuBM";
            resources.ApplyResources(this.menuBM, "menuBM");
            // 
            // menuBMinside
            // 
            this.menuBMinside.Name = "menuBMinside";
            resources.ApplyResources(this.menuBMinside, "menuBMinside");
            this.menuBMinside.Click += new System.EventHandler(this.menuBMinside_Click);
            // 
            // menuBMbefore
            // 
            this.menuBMbefore.Name = "menuBMbefore";
            resources.ApplyResources(this.menuBMbefore, "menuBMbefore");
            this.menuBMbefore.Click += new System.EventHandler(this.menuBMbefore_Click);
            // 
            // menuBMafter
            // 
            this.menuBMafter.Name = "menuBMafter";
            resources.ApplyResources(this.menuBMafter, "menuBMafter");
            this.menuBMafter.Click += new System.EventHandler(this.menuBMafter_Click);
            // 
            // menuBMdelete
            // 
            this.menuBMdelete.Name = "menuBMdelete";
            resources.ApplyResources(this.menuBMdelete, "menuBMdelete");
            this.menuBMdelete.Click += new System.EventHandler(this.menuBMdelete_Click);
            // 
            // menuBMup
            // 
            this.menuBMup.Name = "menuBMup";
            resources.ApplyResources(this.menuBMup, "menuBMup");
            this.menuBMup.Click += new System.EventHandler(this.menuBMup_Click);
            // 
            // menuBMdown
            // 
            this.menuBMdown.Name = "menuBMdown";
            resources.ApplyResources(this.menuBMdown, "menuBMdown");
            this.menuBMdown.Click += new System.EventHandler(this.menuBMdown_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // BookmarksForm
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "BookmarksForm";
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BookmarksForm_FormClosing);
            this.SizeChanged += new System.EventHandler(this.BookmarksForm_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.BookmarksForm_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuBMEdit.ResumeLayout(false);
            this.menuBM.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void toolbar_ButtonClick(int ImageIndex)
		{
			try {
			ToolBarButton b=new ToolBarButton();
			b.ImageIndex=ImageIndex;
			ToolBarButtonClickEventArgs e=new ToolBarButtonClickEventArgs(b);
			toolbar_ButtonClick(null,e);
		} catch {}
		}

		private void toolbar_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			try {
			switch (e.Button.ImageIndex)
			{
				case 0:
				case 1:
				case 2:
				{
					// new
					TreeNode newNode=new TreeNode(Startup.rm.GetString("newnode"),2,2);
					int i=0;
					if (e.Button.ImageIndex==0)
					{
						//add child node
						i= tvwbm.SelectedNode.Nodes.Add(newNode);
						tvwbm.SelectedNode=tvwbm.SelectedNode.Nodes[i];
					}
					else
					{
						//insert sibling node
						i=tvwbm.SelectedNode.Index;
						tvwbm.SelectedNode.Parent.Nodes.Insert(
							(e.Button.ImageIndex==1) ? i : i+1,newNode);
						tvwbm.SelectedNode=tvwbm.SelectedNode.Parent.Nodes[
							(e.Button.ImageIndex==1) ? i : i+1];
					}

					hashRef[tvwbm.SelectedNode.GetHashCode()]="";
					hashText[tvwbm.SelectedNode.GetHashCode()]="";
					UpdateImages(tvwbm.Nodes[0]);
					tvwbm.SelectedNode.BeginEdit();
					break;
				}

				case 3: // delete bookmark
				{
					DialogResult r=DialogResult.Yes;
					if (tvwbm.SelectedNode.Nodes.Count>0)
					{
						r=MessageBox.Show(
							Startup.rm.GetString("confirmdeletebm"),
							"VulSearch 4",MessageBoxButtons.YesNo);
					}
					if (r==DialogResult.Yes)
					{
						KillHashes(tvwbm.SelectedNode);
						tvwbm.SelectedNode.Remove();
					}
					
					break;
				}

				case 4: // move up
				{
					TreeNode p=tvwbm.SelectedNode;
					int k=p.Index;
					TreeNode q=p.Parent;
					q.Nodes.Remove(p);
					q.Nodes.Insert(k-1,p);
					tvwbm.SelectedNode=p;
					break;
				}

				case 5: // move down
				{
					TreeNode p=tvwbm.SelectedNode;
					int k=p.Index;
					TreeNode q=p.Parent;
					q.Nodes.Remove(p);
					q.Nodes.Insert(k+1,p);
					tvwbm.SelectedNode=p;
					break;
				}

					

				case 6:  // bold
				{
					FontStyle f=rtbDesc.SelectionFont.Style;
					if (((int) f) == (((int) f) | ((int) FontStyle.Bold)))
						f&= ~FontStyle.Bold;
					else
						f|= FontStyle.Bold;
					rtbDesc.SelectionFont=new Font(rtbDesc.Font.Name,rtbDesc.Font.SizeInPoints,f);
					break;
				}

				case 7:
				{
					FontStyle f=rtbDesc.SelectionFont.Style;
					if (((int) f) == (((int) f) | ((int) FontStyle.Italic)))
						f&= ~FontStyle.Italic;
					else
						f|= FontStyle.Italic;
					rtbDesc.SelectionFont=new Font(rtbDesc.Font.Name,rtbDesc.Font.SizeInPoints,f);
					break;
				}

				case 8:
					// search
				{
					Startup.SearchBMForm.Visible=tlbSearch.Pushed;
					break;
				}
			}
		} catch {}
		}

		private void BookmarksForm_SizeChanged(object sender, System.EventArgs e)
		{
			try {
			/*
			if (toolbar.Visible)
			{
				tvwbm.Top=toolbar.Height;
			}
			else
			{
				tvwbm.Top=5;
			}
			*/
		} catch {}
		}

		private void tvwbm_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			try {
			txtRef.Text=(string) hashRef[e.Node.GetHashCode()];
			rtbDesc.Rtf=(string) hashText[e.Node.GetHashCode()];
			tlbInsert.Enabled=(tvwbm.SelectedNode!=tvwbm.Nodes[0]);
			tlbAfter.Enabled=(tvwbm.SelectedNode!=tvwbm.Nodes[0]);
			tlbDelete.Enabled=(tvwbm.SelectedNode!=tvwbm.Nodes[0]);
			tlbMoveUp.Enabled=(tvwbm.SelectedNode.Index>0);
			tlbMoveDown.Enabled=(tvwbm.SelectedNode!=tvwbm.Nodes[0] && tvwbm.SelectedNode.Index<tvwbm.SelectedNode.Parent.Nodes.Count-1);
		} catch {}
		}

		private void txtRef_TextChanged(object sender, System.EventArgs e)
		{
			try {
			hashRef[tvwbm.SelectedNode.GetHashCode()]=txtRef.Text;
		} catch {}
		}

		private void rtbDesc_TextChanged(object sender, System.EventArgs e)
		{
			try {
			hashText[tvwbm.SelectedNode.GetHashCode()]=rtbDesc.Rtf;
		} catch {}
		}

		public void UpdateImages(TreeNode n)
		{
			try {
			if (n!= tvwbm.Nodes[0])
			{
				if (n.Nodes.Count==0)
				{
					n.ImageIndex=1;
					n.SelectedImageIndex=1;
				}
				else
				{
					n.ImageIndex=2;
					n.SelectedImageIndex=2;
				}
			}

			foreach (TreeNode m in n.Nodes)
			{
				UpdateImages(m);
			}
		} catch {}
		}

		private void KillHashes(TreeNode n)
		{
			try {
			hashRef.Remove(n.GetHashCode());
			hashText.Remove(n.GetHashCode());
			foreach (TreeNode m in n.Nodes)
			{
				KillHashes(m);
			}
		} catch {}
		}
 
		private void tvwbm_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e)
		{ 
			try {
			DoDragDrop(e.Item, DragDropEffects.Move | DragDropEffects.Copy );
		} catch {}
		} 

 
		private void tvwbm_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{ 
			try {
			if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
				e.Effect = DragDropEffects.Move;
			else if (e.Data.GetDataPresent("System.String",false))
				e.Effect=DragDropEffects.Copy;
			else
				e.Effect=DragDropEffects.None;
		} catch {}
		}

 
		private void tvwbm_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{
			try {
			Point pt = tvwbm.PointToClient(new Point(e.X,e.Y)); 
			tvwbm.SelectedNode=tvwbm.GetNodeAt(pt);
			if (tvwbm.SelectedNode != dragOver)
			{
				dragOver=tvwbm.SelectedNode;
				tmrDragOver.Stop();
				tmrDragOver.Start();
			}

			int delta = tvwbm.Height - pt.Y;
			if ((delta < tvwbm.Height / 2) && (delta > 0))
			{
				TreeNode tn = tvwbm.GetNodeAt(pt.X, pt.Y);
				if (tn.NextVisibleNode != null)
					tn.NextVisibleNode.EnsureVisible();
			}
			if ((delta > tvwbm.Height / 2) && (delta < tvwbm.Height)) 
			{ 
				TreeNode tn = tvwbm.GetNodeAt(pt.X, pt.Y); 
				if (tn.PrevVisibleNode != null) 
					tn.PrevVisibleNode.EnsureVisible(); 
			}
		} catch {}
		}

		private void tvwbm_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			try {
			tmrDragOver.Stop();
			TreeNode newNode=null;
			bool killNode=false;
			string _ref="",desc="";
			if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
			{
				newNode = (TreeNode) e.Data.GetData(typeof(System.Windows.Forms.TreeNode));
				killNode=true;
			}
			if (e.Data.GetDataPresent("System.String", false))
			{
				string s=(string) e.Data.GetData("System.String");
				if (s.IndexOf("\n")>=0)
				{
					_ref=s.Substring(0,s.IndexOf("\n"));
					desc=s.Substring(s.IndexOf("\n")+1);
				}
				else
				{
					_ref=s;
					desc="";
				}
				RichTextBox rtb=new RichTextBox();
				rtb.SelectedText=desc;
				desc=rtb.Rtf;
				rtb=null;
				newNode=new TreeNode(_ref);
			}
			
			if (newNode != null)
			{
				Point pt;
				TreeNode destinationNode;
				pt = tvwbm.PointToClient(new Point(e.X, e.Y));
				destinationNode = tvwbm.GetNodeAt(pt);
				if (destinationNode!= null && destinationNode!=newNode)
				{ 
					if (killNode) tvwbm.Nodes.Remove(newNode);
					if (!killNode)
					{
						hashRef[newNode.GetHashCode()]=_ref;
						hashText[newNode.GetHashCode()]=desc;
					}
					destinationNode.Nodes.Add(newNode);
					destinationNode.Expand();
				}
			}
			UpdateImages(tvwbm.Nodes[0]);
		} catch {}
		}


		private void tmrDragOver_Tick(object sender, System.EventArgs e)
		{
			try {
			tmrDragOver.Stop();
			if (dragOver!=null && dragOver.Nodes.Count>0) dragOver.Nodes[0].EnsureVisible();
		} catch {}
		}

		private void txtRef_DoubleClick(object sender, System.EventArgs e)
		{
			try {
			string s=";" + txtRef.Text +";";
			int i=txtRef.SelectionStart+1;
			int j=s.LastIndexOf(";",i,i+1);
			int k=s.IndexOf(";",i+1);
			s=s.Substring(j+1,k-j-1).Trim();
			Startup.MainForm.JumpToRef(s);
		} catch {}
		}

		private void btnAddRef_Click(object sender, System.EventArgs e)
		{
			try {
			AddRefForm f=new AddRefForm();
			f.ShowDialog();
			if (f.Reference!="x")
			{
				txtRef.Text=txtRef.Text.TrimEnd();
				if (txtRef.Text.Length>0 && !txtRef.Text.EndsWith(";"))
					txtRef.Text+=" ; ";
				txtRef.Text+= f.Reference;
			}
			f=null;
		} catch {}
		}



        private void BookmarksForm_VisibleChanged(object sender, EventArgs e)
        {
            Startup.MainForm.toolBookmarks.Checked = this.Visible;
        }

        private void BookmarksForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }

        private void menuBMEditCut_Click(object sender, EventArgs e)
        {
            menuBMEditCopy_Click(sender, e);
            try
            {
               ToolStripMenuItem x = sender as ToolStripMenuItem;
                ContextMenuStrip y = x.Owner as ContextMenuStrip;
                RichTextBox rtb = y.SourceControl as RichTextBox;
                rtb.SelectedText = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to cut!\n" + ex.ToString(), "Cut error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void menuBMEditCopy_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem x = sender as ToolStripMenuItem;
                ContextMenuStrip y = x.Owner as ContextMenuStrip;
                RichTextBox rtb = y.SourceControl as RichTextBox;
                DataObject copy = new DataObject();
                // Place text version in DataObject as text and unicode
                copy.SetData(DataFormats.Text, rtb.SelectedText);
                copy.SetData(DataFormats.UnicodeText, rtb.SelectedText);
                copy.SetData(DataFormats.Rtf, rtb.SelectedRtf);
                Clipboard.SetDataObject(copy, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to copy to clipboard!\n" + ex.ToString(), "Copy error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void menuBMEditPaste_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem x = sender as ToolStripMenuItem;
                ContextMenuStrip y = x.Owner as ContextMenuStrip;
                RichTextBox rtb = y.SourceControl as RichTextBox;

                IDataObject o = Clipboard.GetDataObject();
                if (o.GetDataPresent(DataFormats.Rtf))
                    rtb.SelectedRtf = (string)o.GetData(DataFormats.Rtf);
                else if (o.GetDataPresent(DataFormats.Text))
                    rtb.SelectedText = (string)o.GetData(DataFormats.Rtf);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to pase from clipboard!\n" + ex.ToString(), "Paste error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void menuBMinside_Click(object sender, EventArgs e)
        {
            toolbar_ButtonClick(0);
        }

        private void menuBMbefore_Click(object sender, EventArgs e)
        {
            toolbar_ButtonClick(1);

        }

        private void menuBMafter_Click(object sender, EventArgs e)
        {
            toolbar_ButtonClick(2);

        }

        private void menuBMdelete_Click(object sender, EventArgs e)
        {
            toolbar_ButtonClick(3);

        }

        private void menuBMup_Click(object sender, EventArgs e)
        {
            toolbar_ButtonClick(4);

        }

        private void menuBMdown_Click(object sender, EventArgs e)
        {
            toolbar_ButtonClick(5);

        }
	}
}
