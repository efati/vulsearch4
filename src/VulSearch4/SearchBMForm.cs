using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for SearchBMForm.
	/// </summary>
	public class SearchBMForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ColumnHeader colName;
		private System.Windows.Forms.ColumnHeader colRef;
		private System.Windows.Forms.ColumnHeader colPath;
		private System.Windows.Forms.Button btnSearch;
		public System.Windows.Forms.ListView lvres;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox txtSearch3;
		private System.Windows.Forms.TextBox txtSearch2;
		private System.Windows.Forms.TextBox txtSearch1;
		private System.Windows.Forms.ComboBox cmbAndOr2;
		private System.Windows.Forms.ComboBox cmbAndOr3;
		private System.Windows.Forms.ComboBox cmbSearch1;
		private System.Windows.Forms.ComboBox cmbSearch3;
		private System.Windows.Forms.ComboBox cmbSearch2;
		private System.Windows.Forms.Label label1;
		private ArrayList nodes = new ArrayList();

		public SearchBMForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			cmbSearch1.SelectedIndex=0;
			cmbSearch2.SelectedIndex=1;
			cmbSearch3.SelectedIndex=2;
			cmbAndOr2.SelectedIndex=0;
			cmbAndOr3.SelectedIndex=0;

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SearchBMForm));
			this.lvres = new System.Windows.Forms.ListView();
			this.colName = new System.Windows.Forms.ColumnHeader();
			this.colRef = new System.Windows.Forms.ColumnHeader();
			this.colPath = new System.Windows.Forms.ColumnHeader();
			this.txtSearch3 = new System.Windows.Forms.TextBox();
			this.btnSearch = new System.Windows.Forms.Button();
			this.txtSearch2 = new System.Windows.Forms.TextBox();
			this.txtSearch1 = new System.Windows.Forms.TextBox();
			this.cmbAndOr2 = new System.Windows.Forms.ComboBox();
			this.cmbAndOr3 = new System.Windows.Forms.ComboBox();
			this.cmbSearch1 = new System.Windows.Forms.ComboBox();
			this.cmbSearch3 = new System.Windows.Forms.ComboBox();
			this.cmbSearch2 = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lvres
			// 
			this.lvres.AccessibleDescription = resources.GetString("lvres.AccessibleDescription");
			this.lvres.AccessibleName = resources.GetString("lvres.AccessibleName");
			this.lvres.Alignment = ((System.Windows.Forms.ListViewAlignment)(resources.GetObject("lvres.Alignment")));
			this.lvres.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lvres.Anchor")));
			this.lvres.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lvres.BackgroundImage")));
			this.lvres.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																																						this.colName,
																																						this.colRef,
																																						this.colPath});
			this.lvres.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lvres.Dock")));
			this.lvres.Enabled = ((bool)(resources.GetObject("lvres.Enabled")));
			this.lvres.Font = ((System.Drawing.Font)(resources.GetObject("lvres.Font")));
			this.lvres.HideSelection = false;
			this.lvres.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lvres.ImeMode")));
			this.lvres.LabelWrap = ((bool)(resources.GetObject("lvres.LabelWrap")));
			this.lvres.Location = ((System.Drawing.Point)(resources.GetObject("lvres.Location")));
			this.lvres.MultiSelect = false;
			this.lvres.Name = "lvres";
			this.lvres.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lvres.RightToLeft")));
			this.lvres.Size = ((System.Drawing.Size)(resources.GetObject("lvres.Size")));
			this.lvres.TabIndex = ((int)(resources.GetObject("lvres.TabIndex")));
			this.lvres.Text = resources.GetString("lvres.Text");
			this.lvres.View = System.Windows.Forms.View.Details;
			this.lvres.Visible = ((bool)(resources.GetObject("lvres.Visible")));
			this.lvres.Click += new System.EventHandler(this.lvres_Click);
			this.lvres.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvres_ColumnClick);
			// 
			// colName
			// 
			this.colName.Text = resources.GetString("colName.Text");
			this.colName.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("colName.TextAlign")));
			this.colName.Width = ((int)(resources.GetObject("colName.Width")));
			// 
			// colRef
			// 
			this.colRef.Text = resources.GetString("colRef.Text");
			this.colRef.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("colRef.TextAlign")));
			this.colRef.Width = ((int)(resources.GetObject("colRef.Width")));
			// 
			// colPath
			// 
			this.colPath.Text = resources.GetString("colPath.Text");
			this.colPath.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("colPath.TextAlign")));
			this.colPath.Width = ((int)(resources.GetObject("colPath.Width")));
			// 
			// txtSearch3
			// 
			this.txtSearch3.AccessibleDescription = resources.GetString("txtSearch3.AccessibleDescription");
			this.txtSearch3.AccessibleName = resources.GetString("txtSearch3.AccessibleName");
			this.txtSearch3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSearch3.Anchor")));
			this.txtSearch3.AutoSize = ((bool)(resources.GetObject("txtSearch3.AutoSize")));
			this.txtSearch3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSearch3.BackgroundImage")));
			this.txtSearch3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSearch3.Dock")));
			this.txtSearch3.Enabled = ((bool)(resources.GetObject("txtSearch3.Enabled")));
			this.txtSearch3.Font = ((System.Drawing.Font)(resources.GetObject("txtSearch3.Font")));
			this.txtSearch3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSearch3.ImeMode")));
			this.txtSearch3.Location = ((System.Drawing.Point)(resources.GetObject("txtSearch3.Location")));
			this.txtSearch3.MaxLength = ((int)(resources.GetObject("txtSearch3.MaxLength")));
			this.txtSearch3.Multiline = ((bool)(resources.GetObject("txtSearch3.Multiline")));
			this.txtSearch3.Name = "txtSearch3";
			this.txtSearch3.PasswordChar = ((char)(resources.GetObject("txtSearch3.PasswordChar")));
			this.txtSearch3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSearch3.RightToLeft")));
			this.txtSearch3.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSearch3.ScrollBars")));
			this.txtSearch3.Size = ((System.Drawing.Size)(resources.GetObject("txtSearch3.Size")));
			this.txtSearch3.TabIndex = ((int)(resources.GetObject("txtSearch3.TabIndex")));
			this.txtSearch3.Text = resources.GetString("txtSearch3.Text");
			this.txtSearch3.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSearch3.TextAlign")));
			this.txtSearch3.Visible = ((bool)(resources.GetObject("txtSearch3.Visible")));
			this.txtSearch3.WordWrap = ((bool)(resources.GetObject("txtSearch3.WordWrap")));
			// 
			// btnSearch
			// 
			this.btnSearch.AccessibleDescription = resources.GetString("btnSearch.AccessibleDescription");
			this.btnSearch.AccessibleName = resources.GetString("btnSearch.AccessibleName");
			this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSearch.Anchor")));
			this.btnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSearch.BackgroundImage")));
			this.btnSearch.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSearch.Dock")));
			this.btnSearch.Enabled = ((bool)(resources.GetObject("btnSearch.Enabled")));
			this.btnSearch.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSearch.FlatStyle")));
			this.btnSearch.Font = ((System.Drawing.Font)(resources.GetObject("btnSearch.Font")));
			this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
			this.btnSearch.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSearch.ImageAlign")));
			this.btnSearch.ImageIndex = ((int)(resources.GetObject("btnSearch.ImageIndex")));
			this.btnSearch.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSearch.ImeMode")));
			this.btnSearch.Location = ((System.Drawing.Point)(resources.GetObject("btnSearch.Location")));
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSearch.RightToLeft")));
			this.btnSearch.Size = ((System.Drawing.Size)(resources.GetObject("btnSearch.Size")));
			this.btnSearch.TabIndex = ((int)(resources.GetObject("btnSearch.TabIndex")));
			this.btnSearch.Text = resources.GetString("btnSearch.Text");
			this.btnSearch.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSearch.TextAlign")));
			this.btnSearch.Visible = ((bool)(resources.GetObject("btnSearch.Visible")));
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// txtSearch2
			// 
			this.txtSearch2.AccessibleDescription = resources.GetString("txtSearch2.AccessibleDescription");
			this.txtSearch2.AccessibleName = resources.GetString("txtSearch2.AccessibleName");
			this.txtSearch2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSearch2.Anchor")));
			this.txtSearch2.AutoSize = ((bool)(resources.GetObject("txtSearch2.AutoSize")));
			this.txtSearch2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSearch2.BackgroundImage")));
			this.txtSearch2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSearch2.Dock")));
			this.txtSearch2.Enabled = ((bool)(resources.GetObject("txtSearch2.Enabled")));
			this.txtSearch2.Font = ((System.Drawing.Font)(resources.GetObject("txtSearch2.Font")));
			this.txtSearch2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSearch2.ImeMode")));
			this.txtSearch2.Location = ((System.Drawing.Point)(resources.GetObject("txtSearch2.Location")));
			this.txtSearch2.MaxLength = ((int)(resources.GetObject("txtSearch2.MaxLength")));
			this.txtSearch2.Multiline = ((bool)(resources.GetObject("txtSearch2.Multiline")));
			this.txtSearch2.Name = "txtSearch2";
			this.txtSearch2.PasswordChar = ((char)(resources.GetObject("txtSearch2.PasswordChar")));
			this.txtSearch2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSearch2.RightToLeft")));
			this.txtSearch2.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSearch2.ScrollBars")));
			this.txtSearch2.Size = ((System.Drawing.Size)(resources.GetObject("txtSearch2.Size")));
			this.txtSearch2.TabIndex = ((int)(resources.GetObject("txtSearch2.TabIndex")));
			this.txtSearch2.Text = resources.GetString("txtSearch2.Text");
			this.txtSearch2.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSearch2.TextAlign")));
			this.txtSearch2.Visible = ((bool)(resources.GetObject("txtSearch2.Visible")));
			this.txtSearch2.WordWrap = ((bool)(resources.GetObject("txtSearch2.WordWrap")));
			// 
			// txtSearch1
			// 
			this.txtSearch1.AccessibleDescription = resources.GetString("txtSearch1.AccessibleDescription");
			this.txtSearch1.AccessibleName = resources.GetString("txtSearch1.AccessibleName");
			this.txtSearch1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSearch1.Anchor")));
			this.txtSearch1.AutoSize = ((bool)(resources.GetObject("txtSearch1.AutoSize")));
			this.txtSearch1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSearch1.BackgroundImage")));
			this.txtSearch1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSearch1.Dock")));
			this.txtSearch1.Enabled = ((bool)(resources.GetObject("txtSearch1.Enabled")));
			this.txtSearch1.Font = ((System.Drawing.Font)(resources.GetObject("txtSearch1.Font")));
			this.txtSearch1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSearch1.ImeMode")));
			this.txtSearch1.Location = ((System.Drawing.Point)(resources.GetObject("txtSearch1.Location")));
			this.txtSearch1.MaxLength = ((int)(resources.GetObject("txtSearch1.MaxLength")));
			this.txtSearch1.Multiline = ((bool)(resources.GetObject("txtSearch1.Multiline")));
			this.txtSearch1.Name = "txtSearch1";
			this.txtSearch1.PasswordChar = ((char)(resources.GetObject("txtSearch1.PasswordChar")));
			this.txtSearch1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSearch1.RightToLeft")));
			this.txtSearch1.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSearch1.ScrollBars")));
			this.txtSearch1.Size = ((System.Drawing.Size)(resources.GetObject("txtSearch1.Size")));
			this.txtSearch1.TabIndex = ((int)(resources.GetObject("txtSearch1.TabIndex")));
			this.txtSearch1.Text = resources.GetString("txtSearch1.Text");
			this.txtSearch1.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSearch1.TextAlign")));
			this.txtSearch1.Visible = ((bool)(resources.GetObject("txtSearch1.Visible")));
			this.txtSearch1.WordWrap = ((bool)(resources.GetObject("txtSearch1.WordWrap")));
			// 
			// cmbAndOr2
			// 
			this.cmbAndOr2.AccessibleDescription = resources.GetString("cmbAndOr2.AccessibleDescription");
			this.cmbAndOr2.AccessibleName = resources.GetString("cmbAndOr2.AccessibleName");
			this.cmbAndOr2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbAndOr2.Anchor")));
			this.cmbAndOr2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbAndOr2.BackgroundImage")));
			this.cmbAndOr2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbAndOr2.Dock")));
			this.cmbAndOr2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbAndOr2.Enabled = ((bool)(resources.GetObject("cmbAndOr2.Enabled")));
			this.cmbAndOr2.Font = ((System.Drawing.Font)(resources.GetObject("cmbAndOr2.Font")));
			this.cmbAndOr2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbAndOr2.ImeMode")));
			this.cmbAndOr2.IntegralHeight = ((bool)(resources.GetObject("cmbAndOr2.IntegralHeight")));
			this.cmbAndOr2.ItemHeight = ((int)(resources.GetObject("cmbAndOr2.ItemHeight")));
			this.cmbAndOr2.Items.AddRange(new object[] {
																									 resources.GetString("cmbAndOr2.Items"),
																									 resources.GetString("cmbAndOr2.Items1")});
			this.cmbAndOr2.Location = ((System.Drawing.Point)(resources.GetObject("cmbAndOr2.Location")));
			this.cmbAndOr2.MaxDropDownItems = ((int)(resources.GetObject("cmbAndOr2.MaxDropDownItems")));
			this.cmbAndOr2.MaxLength = ((int)(resources.GetObject("cmbAndOr2.MaxLength")));
			this.cmbAndOr2.Name = "cmbAndOr2";
			this.cmbAndOr2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbAndOr2.RightToLeft")));
			this.cmbAndOr2.Size = ((System.Drawing.Size)(resources.GetObject("cmbAndOr2.Size")));
			this.cmbAndOr2.TabIndex = ((int)(resources.GetObject("cmbAndOr2.TabIndex")));
			this.cmbAndOr2.Text = resources.GetString("cmbAndOr2.Text");
			this.cmbAndOr2.Visible = ((bool)(resources.GetObject("cmbAndOr2.Visible")));
			// 
			// cmbAndOr3
			// 
			this.cmbAndOr3.AccessibleDescription = resources.GetString("cmbAndOr3.AccessibleDescription");
			this.cmbAndOr3.AccessibleName = resources.GetString("cmbAndOr3.AccessibleName");
			this.cmbAndOr3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbAndOr3.Anchor")));
			this.cmbAndOr3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbAndOr3.BackgroundImage")));
			this.cmbAndOr3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbAndOr3.Dock")));
			this.cmbAndOr3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbAndOr3.Enabled = ((bool)(resources.GetObject("cmbAndOr3.Enabled")));
			this.cmbAndOr3.Font = ((System.Drawing.Font)(resources.GetObject("cmbAndOr3.Font")));
			this.cmbAndOr3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbAndOr3.ImeMode")));
			this.cmbAndOr3.IntegralHeight = ((bool)(resources.GetObject("cmbAndOr3.IntegralHeight")));
			this.cmbAndOr3.ItemHeight = ((int)(resources.GetObject("cmbAndOr3.ItemHeight")));
			this.cmbAndOr3.Items.AddRange(new object[] {
																									 resources.GetString("cmbAndOr3.Items"),
																									 resources.GetString("cmbAndOr3.Items1")});
			this.cmbAndOr3.Location = ((System.Drawing.Point)(resources.GetObject("cmbAndOr3.Location")));
			this.cmbAndOr3.MaxDropDownItems = ((int)(resources.GetObject("cmbAndOr3.MaxDropDownItems")));
			this.cmbAndOr3.MaxLength = ((int)(resources.GetObject("cmbAndOr3.MaxLength")));
			this.cmbAndOr3.Name = "cmbAndOr3";
			this.cmbAndOr3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbAndOr3.RightToLeft")));
			this.cmbAndOr3.Size = ((System.Drawing.Size)(resources.GetObject("cmbAndOr3.Size")));
			this.cmbAndOr3.TabIndex = ((int)(resources.GetObject("cmbAndOr3.TabIndex")));
			this.cmbAndOr3.Text = resources.GetString("cmbAndOr3.Text");
			this.cmbAndOr3.Visible = ((bool)(resources.GetObject("cmbAndOr3.Visible")));
			// 
			// cmbSearch1
			// 
			this.cmbSearch1.AccessibleDescription = resources.GetString("cmbSearch1.AccessibleDescription");
			this.cmbSearch1.AccessibleName = resources.GetString("cmbSearch1.AccessibleName");
			this.cmbSearch1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbSearch1.Anchor")));
			this.cmbSearch1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbSearch1.BackgroundImage")));
			this.cmbSearch1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbSearch1.Dock")));
			this.cmbSearch1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbSearch1.Enabled = ((bool)(resources.GetObject("cmbSearch1.Enabled")));
			this.cmbSearch1.Font = ((System.Drawing.Font)(resources.GetObject("cmbSearch1.Font")));
			this.cmbSearch1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbSearch1.ImeMode")));
			this.cmbSearch1.IntegralHeight = ((bool)(resources.GetObject("cmbSearch1.IntegralHeight")));
			this.cmbSearch1.ItemHeight = ((int)(resources.GetObject("cmbSearch1.ItemHeight")));
			this.cmbSearch1.Items.AddRange(new object[] {
																										resources.GetString("cmbSearch1.Items"),
																										resources.GetString("cmbSearch1.Items1"),
																										resources.GetString("cmbSearch1.Items2")});
			this.cmbSearch1.Location = ((System.Drawing.Point)(resources.GetObject("cmbSearch1.Location")));
			this.cmbSearch1.MaxDropDownItems = ((int)(resources.GetObject("cmbSearch1.MaxDropDownItems")));
			this.cmbSearch1.MaxLength = ((int)(resources.GetObject("cmbSearch1.MaxLength")));
			this.cmbSearch1.Name = "cmbSearch1";
			this.cmbSearch1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbSearch1.RightToLeft")));
			this.cmbSearch1.Size = ((System.Drawing.Size)(resources.GetObject("cmbSearch1.Size")));
			this.cmbSearch1.TabIndex = ((int)(resources.GetObject("cmbSearch1.TabIndex")));
			this.cmbSearch1.Text = resources.GetString("cmbSearch1.Text");
			this.cmbSearch1.Visible = ((bool)(resources.GetObject("cmbSearch1.Visible")));
			// 
			// cmbSearch3
			// 
			this.cmbSearch3.AccessibleDescription = resources.GetString("cmbSearch3.AccessibleDescription");
			this.cmbSearch3.AccessibleName = resources.GetString("cmbSearch3.AccessibleName");
			this.cmbSearch3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbSearch3.Anchor")));
			this.cmbSearch3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbSearch3.BackgroundImage")));
			this.cmbSearch3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbSearch3.Dock")));
			this.cmbSearch3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbSearch3.Enabled = ((bool)(resources.GetObject("cmbSearch3.Enabled")));
			this.cmbSearch3.Font = ((System.Drawing.Font)(resources.GetObject("cmbSearch3.Font")));
			this.cmbSearch3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbSearch3.ImeMode")));
			this.cmbSearch3.IntegralHeight = ((bool)(resources.GetObject("cmbSearch3.IntegralHeight")));
			this.cmbSearch3.ItemHeight = ((int)(resources.GetObject("cmbSearch3.ItemHeight")));
			this.cmbSearch3.Items.AddRange(new object[] {
																										resources.GetString("cmbSearch3.Items"),
																										resources.GetString("cmbSearch3.Items1"),
																										resources.GetString("cmbSearch3.Items2")});
			this.cmbSearch3.Location = ((System.Drawing.Point)(resources.GetObject("cmbSearch3.Location")));
			this.cmbSearch3.MaxDropDownItems = ((int)(resources.GetObject("cmbSearch3.MaxDropDownItems")));
			this.cmbSearch3.MaxLength = ((int)(resources.GetObject("cmbSearch3.MaxLength")));
			this.cmbSearch3.Name = "cmbSearch3";
			this.cmbSearch3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbSearch3.RightToLeft")));
			this.cmbSearch3.Size = ((System.Drawing.Size)(resources.GetObject("cmbSearch3.Size")));
			this.cmbSearch3.TabIndex = ((int)(resources.GetObject("cmbSearch3.TabIndex")));
			this.cmbSearch3.Text = resources.GetString("cmbSearch3.Text");
			this.cmbSearch3.Visible = ((bool)(resources.GetObject("cmbSearch3.Visible")));
			// 
			// cmbSearch2
			// 
			this.cmbSearch2.AccessibleDescription = resources.GetString("cmbSearch2.AccessibleDescription");
			this.cmbSearch2.AccessibleName = resources.GetString("cmbSearch2.AccessibleName");
			this.cmbSearch2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbSearch2.Anchor")));
			this.cmbSearch2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbSearch2.BackgroundImage")));
			this.cmbSearch2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbSearch2.Dock")));
			this.cmbSearch2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbSearch2.Enabled = ((bool)(resources.GetObject("cmbSearch2.Enabled")));
			this.cmbSearch2.Font = ((System.Drawing.Font)(resources.GetObject("cmbSearch2.Font")));
			this.cmbSearch2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbSearch2.ImeMode")));
			this.cmbSearch2.IntegralHeight = ((bool)(resources.GetObject("cmbSearch2.IntegralHeight")));
			this.cmbSearch2.ItemHeight = ((int)(resources.GetObject("cmbSearch2.ItemHeight")));
			this.cmbSearch2.Items.AddRange(new object[] {
																										resources.GetString("cmbSearch2.Items"),
																										resources.GetString("cmbSearch2.Items1"),
																										resources.GetString("cmbSearch2.Items2")});
			this.cmbSearch2.Location = ((System.Drawing.Point)(resources.GetObject("cmbSearch2.Location")));
			this.cmbSearch2.MaxDropDownItems = ((int)(resources.GetObject("cmbSearch2.MaxDropDownItems")));
			this.cmbSearch2.MaxLength = ((int)(resources.GetObject("cmbSearch2.MaxLength")));
			this.cmbSearch2.Name = "cmbSearch2";
			this.cmbSearch2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbSearch2.RightToLeft")));
			this.cmbSearch2.Size = ((System.Drawing.Size)(resources.GetObject("cmbSearch2.Size")));
			this.cmbSearch2.TabIndex = ((int)(resources.GetObject("cmbSearch2.TabIndex")));
			this.cmbSearch2.Text = resources.GetString("cmbSearch2.Text");
			this.cmbSearch2.Visible = ((bool)(resources.GetObject("cmbSearch2.Visible")));
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// SearchBMForm
			// 
			this.AcceptButton = this.btnSearch;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cmbSearch2);
			this.Controls.Add(this.cmbSearch3);
			this.Controls.Add(this.cmbSearch1);
			this.Controls.Add(this.cmbAndOr3);
			this.Controls.Add(this.cmbAndOr2);
			this.Controls.Add(this.txtSearch1);
			this.Controls.Add(this.txtSearch2);
			this.Controls.Add(this.btnSearch);
			this.Controls.Add(this.txtSearch3);
			this.Controls.Add(this.lvres);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "SearchBMForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.TopMost = true;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.SearchBMForm_Closing);
			this.VisibleChanged += new System.EventHandler(this.SearchBMForm_VisibleChanged);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			try {
			lvres.Items.Clear();
			nodes.Clear();
			DoSearch(Startup.BookmarksForm.tvwbm.Nodes[0]);
		} catch {}
		}

		private bool FoundInNode(TreeNode n, int what, string s)
		{
			try {
			switch (what)
			{
				case 0: // name
					return (n.Text.IndexOf(s)!=-1);
				case 1: // ref
					return (((string) Startup.BookmarksForm.
						hashRef[n.GetHashCode()]).IndexOf(s)!=-1);
				case 2: // text
					return (((string) Startup.BookmarksForm.
						hashText[n.GetHashCode()]).IndexOf(s)!=-1);
			}
			return false;
            }
            catch { return false;  }
		}

		private void DoSearch(TreeNode n)
		{
			try {
			bool found1=FoundInNode(n,cmbSearch1.SelectedIndex,txtSearch1.Text);
			bool found2=FoundInNode(n,cmbSearch2.SelectedIndex,txtSearch2.Text);
			bool found3=FoundInNode(n,cmbSearch3.SelectedIndex,txtSearch3.Text);
			bool found=found1;
			if (cmbAndOr2.SelectedIndex==0)
				found &= found2;
			else
				found |= found2;
			if (cmbAndOr3.SelectedIndex==0)
				found &= found3;
			else
				found |= found3;

			/*
			if ( ((string) Startup.BookmarksForm.hashRef[n.GetHashCode()])
				.IndexOf(txtRef.Text)!=-1 && 
				((string) Startup.BookmarksForm.hashText[n.GetHashCode()])
				.IndexOf(txtText.Text)!=-1 &&
				n.Text.IndexOf(txtName.Text)!=-1)
				*/

			if (found)
			{
				nodes.Add(n);
				ListViewItem s = new ListViewItem(n.Text);
				s.SubItems.Add((string) Startup.BookmarksForm.hashRef[n.GetHashCode()]);
				string t=n.FullPath;
				if (n==Startup.BookmarksForm.tvwbm.Nodes[0])
				{
					t="";
				}
				else
				{
					t=t.Substring(0,t.LastIndexOf("\\"));
				}
				s.SubItems.Add(t);
				lvres.Items.Add(s);
			}
			foreach (TreeNode m in n.Nodes)
				DoSearch(m);
		} catch {}
		}

		private void SearchBMForm_VisibleChanged(object sender, System.EventArgs e)
		{
			try {
			Startup.BookmarksForm.tlbSearch.Pushed=this.Visible;
		} catch {}
		}

		private void lvres_Click(object sender, System.EventArgs e)
		{
			try {
			if (lvres.SelectedItems.Count>0)
				Startup.BookmarksForm.tvwbm.SelectedNode=
					(nodes[lvres.SelectedIndices[0]] as TreeNode);
		} catch {}
		}

		private void SearchBMForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try {
			e.Cancel=true;
			this.Hide();
		} catch {}
		}

		private void lvres_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			try {
			lvres.ListViewItemSorter=new ListViewItemComparer(e.Column);
			lvres.Sort();
		} catch {}
		}



	}


	class ListViewItemComparer : IComparer 
	{
		private int col;
		public ListViewItemComparer() 
		{
			try {
			col=0;
		} catch {}
		}
		public ListViewItemComparer(int column) 
		{
			try {
			col=column;
		} catch {}
		}
		public int Compare(object x, object y) 
		{
			try {
			return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
            }
            catch { return 0; }
		}
	}

}
