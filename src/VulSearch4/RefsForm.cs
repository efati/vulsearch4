using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for RefsForm.
	/// </summary>
	public class RefsForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnChoose;
		private System.Windows.Forms.Button btnAddRef;
		internal System.Windows.Forms.RichTextBox rtfRefs;
		private System.Windows.Forms.Label label2;
		internal System.Windows.Forms.RadioButton radVerse;
		internal System.Windows.Forms.RadioButton radType;
		internal System.Windows.Forms.ComboBox cmbType;
		private System.Windows.Forms.TextBox txtDesc;
		private System.Windows.Forms.TextBox txtRef;

		private static int ticks;
		public System.Windows.Forms.ImageList imgList;
        private ContextMenuStrip menuRefs;
        private ToolStripMenuItem menuRefsCopy;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem menuRefsEdit;
        private ToolStripMenuItem menuRefsDelete;
		private System.ComponentModel.IContainer components;

        private NewRef curr;

		public RefsForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			ticks=Environment.TickCount;

		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefsForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddRef = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.btnChoose = new System.Windows.Forms.Button();
            this.txtRef = new System.Windows.Forms.TextBox();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rtfRefs = new System.Windows.Forms.RichTextBox();
            this.radVerse = new System.Windows.Forms.RadioButton();
            this.radType = new System.Windows.Forms.RadioButton();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.menuRefs = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuRefsCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRefsEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRefsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1.SuspendLayout();
            this.menuRefs.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnAddRef);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtDesc);
            this.panel1.Controls.Add(this.btnChoose);
            this.panel1.Controls.Add(this.txtRef);
            this.panel1.Controls.Add(this.cmbType);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Name = "panel1";
            // 
            // btnAddRef
            // 
            resources.ApplyResources(this.btnAddRef, "btnAddRef");
            this.btnAddRef.Name = "btnAddRef";
            this.btnAddRef.Click += new System.EventHandler(this.btnAddRef_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtDesc
            // 
            resources.ApplyResources(this.txtDesc, "txtDesc");
            this.txtDesc.Name = "txtDesc";
            // 
            // btnChoose
            // 
            resources.ApplyResources(this.btnChoose, "btnChoose");
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // txtRef
            // 
            resources.ApplyResources(this.txtRef, "txtRef");
            this.txtRef.Name = "txtRef";
            this.txtRef.Enter += new System.EventHandler(this.txtRef_Enter);
            this.txtRef.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRef_KeyUp);
            // 
            // cmbType
            // 
            resources.ApplyResources(this.cmbType, "cmbType");
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.Name = "cmbType";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // rtfRefs
            // 
            resources.ApplyResources(this.rtfRefs, "rtfRefs");
            this.rtfRefs.ContextMenuStrip = this.menuRefs;
            this.rtfRefs.Name = "rtfRefs";
            this.rtfRefs.ReadOnly = true;
            this.rtfRefs.MouseMove += new System.Windows.Forms.MouseEventHandler(this.rtfRefs_MouseMove);
            this.rtfRefs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rtfRefs_MouseUp);
            // 
            // radVerse
            // 
            resources.ApplyResources(this.radVerse, "radVerse");
            this.radVerse.Name = "radVerse";
            this.radVerse.CheckedChanged += new System.EventHandler(this.rad_Change);
            // 
            // radType
            // 
            resources.ApplyResources(this.radType, "radType");
            this.radType.Name = "radType";
            this.radType.CheckedChanged += new System.EventHandler(this.rad_Change);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "");
            // 
            // menuRefs
            // 
            this.menuRefs.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuRefsCopy,
            this.toolStripSeparator1,
            this.menuRefsEdit,
            this.menuRefsDelete});
            this.menuRefs.Name = "menuRefs";
            resources.ApplyResources(this.menuRefs, "menuRefs");
            // 
            // menuRefsCopy
            // 
            this.menuRefsCopy.Name = "menuRefsCopy";
            resources.ApplyResources(this.menuRefsCopy, "menuRefsCopy");
            this.menuRefsCopy.Click += new System.EventHandler(this.menuRefsCopy_Click);
            // 
            // menuRefsEdit
            // 
            this.menuRefsEdit.Name = "menuRefsEdit";
            resources.ApplyResources(this.menuRefsEdit, "menuRefsEdit");
            this.menuRefsEdit.Click += new System.EventHandler(this.menuRefsEdit_Click);
            // 
            // menuRefsDelete
            // 
            this.menuRefsDelete.Name = "menuRefsDelete";
            resources.ApplyResources(this.menuRefsDelete, "menuRefsDelete");
            this.menuRefsDelete.Click += new System.EventHandler(this.menuRefsDelete_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // RefsForm
            // 
            this.AcceptButton = this.btnAddRef;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.radType);
            this.Controls.Add(this.radVerse);
            this.Controls.Add(this.rtfRefs);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "RefsForm";
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RefsForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.RefsForm_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuRefs.ResumeLayout(false);
            this.ResumeLayout(false);
        }
		#endregion

		private void btnAddRef_Click(object sender, System.EventArgs e)
		{
			try {
			if (txtRef.Text=="") return;
			//first calculate hash key.
			string k=Bible.Abbrev[Startup.MainForm.cmbBook.SelectedIndex] + " " + (Startup.MainForm.cmbChapter.SelectedIndex+1).ToString();
			ArrayList a=new ArrayList();
			if (Startup.hashXref[k] != null)
				a=Startup.hashXref[k] as ArrayList;
			a.Add(Startup.ParseNewRef((Startup.MainForm.cmbVerse.SelectedIndex+1).ToString(),txtRef.Text,txtDesc.Text,((RefType) Startup.RefTypes[cmbType.SelectedIndex]).Name,k));
 			Startup.hashXref[k]=a;

			Startup.MainForm.RedisplayRefs();
			txtDesc.Text="";
			txtRef.Text="";
			txtRef.Focus();
		} catch {}
		}

		private void rad_Change(object sender, System.EventArgs e)
		{
			try {
			Startup.MainForm.RedisplayRefs();
		} catch {}
		}


		private void rtfRefs_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			try {
			if (e.Button==MouseButtons.Left && e.Clicks>0)
			{
				if (Environment.TickCount-ticks<300)
				{
					//double click - rtf control ignores double-click, so we have to go through
					//this palaver.
					try
					{
						/*
						string s=" "+rtfRefs.Text+" ";
						int i=rtfRefs.SelectionStart+rtfRefs.SelectionLength+1;
						Regex re=new Regex(@"\s([0-9]|;|--)+\s");
						Regex rf=new Regex("([A-Za-z0-9])+ ([0-9])+:([0-9])+");
						string w="";
						int currind=0;
						foreach(string t in re.Split(s))
						{
							Match m=rf.Match(t);
							currind=s.IndexOf(t,currind);
							if (m.Success && i>=currind)
								w=m.Value;
						}
						if (w=="")
						{
							//no proper ref found - try to find a ref to chpater without verse
							re=new Regex(@"\s\w*\s\d{1,3}\s");
							foreach (Match m in re.Matches(s))
							{
								if (m.Success && i>=s.IndexOf(m.Value))
									w=m.Value.Trim()+":1";
							}
						}
						*/
						string w=GetRefFromPos(rtfRefs.SelectionStart).Ref;
						if (w.IndexOf(":")<1) w+=":1";

						Startup.MainForm.JumpToRef(w);
					}
					catch
					{
						MessageBox.Show(Startup.rm.GetString("invalidref"));
					}

				}
				ticks=Environment.TickCount;
			}


		} catch {}
		}

		private NewRef GetRefFromPos(int pos)
		{
			for (int i=0; i< Startup.MainForm.DisplayRefs.Count; i++)
			{
				DisplayRef r=(DisplayRef) Startup.MainForm.DisplayRefs[i];
				if (r.Position>=pos)
					return r.Ref;
			}
			return new NewRef("BACKREF","","","");
		}

		private void txtRef_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			try {
			if (e.Control && e.KeyCode==Keys.Right)
			{
				if (Startup.MainForm.cmbVerse.SelectedIndex!=Startup.MainForm.cmbVerse.Items.Count-1)
					Startup.MainForm.cmbVerse.SelectedIndex++;
			}
			if (e.Control && e.KeyCode==Keys.Left)
			{
				if (Startup.MainForm.cmbVerse.SelectedIndex!=0)
					Startup.MainForm.cmbVerse.SelectedIndex--;
			}
			txtRef.Focus();
		} catch {}
		}

		private void txtRef_Enter(object sender, System.EventArgs e)
		{
			try {
			txtRef.SelectionStart=0;
			txtRef.SelectionLength=txtRef.Text.Length;
		} catch {}
		}

		private void btnChoose_Click(object sender, System.EventArgs e)
		{
			try {
			AddRefForm f=new AddRefForm(1,1,"",txtDesc.Text);
			f.ShowDialog();
			if (f.Reference!="x")
			{
				txtRef.Text=f.Reference;
				txtDesc.Text=f.Desc;
				btnAddRef_Click(null,null);
			}
			f=null;
		} catch {}
		}

        private void RefsForm_VisibleChanged(object sender, EventArgs e)
        {
            Startup.MainForm.toolXrefs.Checked = this.Visible;
        }

        private void RefsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }

        private void menuRefsCopy_Click(object sender, EventArgs e)
        {
                
            try
            {
                DataObject copy = new DataObject();
                // Place text version in DataObject as text and unicode
                copy.SetData(DataFormats.Text, rtfRefs.SelectedText);
                copy.SetData(DataFormats.UnicodeText, rtfRefs.SelectedText);
                copy.SetData(DataFormats.Rtf, rtfRefs.SelectedRtf);
                Clipboard.SetDataObject(copy, true);
            }
            catch ( Exception ex)
            {
                MessageBox.Show("Failed to copy to clipboard!\n" +  ex.ToString(), "Copy error",  MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void menuRefsEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Regex reg = new Regex(@"(?:Gn|Ex|Lv|Nm|Dt|Jos|Jdc|Rt|1Rg|2Rg|3Rg|4Rg|1Par|2Par|Esr|Neh|Tob|Jdt|Est|Job|Ps|Pr|Ecl|Ct|Sap|Sir|Is|Jr|Lam|Bar|Ez|Dn|Os|Joel|Am|Abd|Jon|Mch|Nah|Hab|Soph|Agg|Zach|Mal|1Mcc|2Mcc|Mt|Mc|Lc|Jo|Act|Rom|1Cor|2Cor|Gal|Eph|Phlp|Col|1Thes|2Thes|1Tim|2Tim|Tit|Phlm|Hbr|Jac|1Ptr|2Ptr|1Jo|2Jo|3Jo|Jud|Apc) \d{1,3}:\d{1,3}");
                int chap = 1, verse = 1, book = 0;
                if (reg.Match(curr.Ref + ":1").Success)
                {
                    string s = reg.Match(curr.Ref + ":1").Value;
                    chap = int.Parse(s.Substring(s.IndexOf(" ") + 1,
                        s.IndexOf(":") - 1 - s.IndexOf(" ")));
                    verse = int.Parse(s.Substring(s.IndexOf(":") + 1));
                    book = 0;
                    s = s.Substring(0, s.IndexOf(" "));
                    for (int i = 0; i < 73; i++)
                    {
                        if (s == Bible.Abbrev[i])
                        {
                            book = i;
                            break;
                        }
                    }
                }

                AddRefForm f = new AddRefForm(book + 1, chap, verse.ToString(), curr.Desc);
                f.ShowDialog();
                if (f.Reference != "x")
                {
                    Startup.MainForm.ChangeRef(curr, f.Reference, f.Desc);
                }
                f = null;
                txtRef.Focus();
            
            }
            catch {}
        }

        private void menuRefsDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Startup.MainForm.RemoveRef(curr);
                txtRef.Focus();
            }
            catch { }
        }

        private void rtfRefs_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {

                curr = GetRefFromPos(rtfRefs.GetCharIndexFromPosition(new Point(e.X, e.Y)));
            }
            catch { }
        }

	}
}
