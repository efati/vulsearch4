using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;

namespace VulSearch4
{
	/// <summary>
	/// Summary description for AddBibleForm.
	/// </summary>
	public class AddBibleForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.FolderBrowserDialog fldBrows;
		public System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.Label label4;
		public System.Windows.Forms.RadioButton radLat;
		public System.Windows.Forms.RadioButton radNotLat;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label5;
		public System.Windows.Forms.TextBox txtDesc;

		public bool WasCancelled=false;
		public System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label label6;
		public System.Windows.Forms.ComboBox cmbExt;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AddBibleForm()
		{
			try {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		} catch {}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			try {
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		} catch {}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AddBibleForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.btnNext = new System.Windows.Forms.Button();
			this.fldBrows = new System.Windows.Forms.FolderBrowserDialog();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.radLat = new System.Windows.Forms.RadioButton();
			this.radNotLat = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmbExt = new System.Windows.Forms.ComboBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.txtDesc = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.AccessibleDescription = resources.GetString("panel1.AccessibleDescription");
			this.panel1.AccessibleName = resources.GetString("panel1.AccessibleName");
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel1.Anchor")));
			this.panel1.AutoScroll = ((bool)(resources.GetObject("panel1.AutoScroll")));
			this.panel1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMargin")));
			this.panel1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMinSize")));
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel1.Dock")));
			this.panel1.Enabled = ((bool)(resources.GetObject("panel1.Enabled")));
			this.panel1.Font = ((System.Drawing.Font)(resources.GetObject("panel1.Font")));
			this.panel1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel1.ImeMode")));
			this.panel1.Location = ((System.Drawing.Point)(resources.GetObject("panel1.Location")));
			this.panel1.Name = "panel1";
			this.panel1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel1.RightToLeft")));
			this.panel1.Size = ((System.Drawing.Size)(resources.GetObject("panel1.Size")));
			this.panel1.TabIndex = ((int)(resources.GetObject("panel1.TabIndex")));
			this.panel1.Text = resources.GetString("panel1.Text");
			this.panel1.Visible = ((bool)(resources.GetObject("panel1.Visible")));
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// label2
			// 
			this.label2.AccessibleDescription = resources.GetString("label2.AccessibleDescription");
			this.label2.AccessibleName = resources.GetString("label2.AccessibleName");
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label2.Anchor")));
			this.label2.AutoSize = ((bool)(resources.GetObject("label2.AutoSize")));
			this.label2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label2.Dock")));
			this.label2.Enabled = ((bool)(resources.GetObject("label2.Enabled")));
			this.label2.Font = ((System.Drawing.Font)(resources.GetObject("label2.Font")));
			this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
			this.label2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.ImageAlign")));
			this.label2.ImageIndex = ((int)(resources.GetObject("label2.ImageIndex")));
			this.label2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label2.ImeMode")));
			this.label2.Location = ((System.Drawing.Point)(resources.GetObject("label2.Location")));
			this.label2.Name = "label2";
			this.label2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label2.RightToLeft")));
			this.label2.Size = ((System.Drawing.Size)(resources.GetObject("label2.Size")));
			this.label2.TabIndex = ((int)(resources.GetObject("label2.TabIndex")));
			this.label2.Text = resources.GetString("label2.Text");
			this.label2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.TextAlign")));
			this.label2.Visible = ((bool)(resources.GetObject("label2.Visible")));
			// 
			// btnNext
			// 
			this.btnNext.AccessibleDescription = resources.GetString("btnNext.AccessibleDescription");
			this.btnNext.AccessibleName = resources.GetString("btnNext.AccessibleName");
			this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNext.Anchor")));
			this.btnNext.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNext.BackgroundImage")));
			this.btnNext.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNext.Dock")));
			this.btnNext.Enabled = ((bool)(resources.GetObject("btnNext.Enabled")));
			this.btnNext.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNext.FlatStyle")));
			this.btnNext.Font = ((System.Drawing.Font)(resources.GetObject("btnNext.Font")));
			this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
			this.btnNext.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNext.ImageAlign")));
			this.btnNext.ImageIndex = ((int)(resources.GetObject("btnNext.ImageIndex")));
			this.btnNext.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNext.ImeMode")));
			this.btnNext.Location = ((System.Drawing.Point)(resources.GetObject("btnNext.Location")));
			this.btnNext.Name = "btnNext";
			this.btnNext.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNext.RightToLeft")));
			this.btnNext.Size = ((System.Drawing.Size)(resources.GetObject("btnNext.Size")));
			this.btnNext.TabIndex = ((int)(resources.GetObject("btnNext.TabIndex")));
			this.btnNext.Text = resources.GetString("btnNext.Text");
			this.btnNext.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNext.TextAlign")));
			this.btnNext.Visible = ((bool)(resources.GetObject("btnNext.Visible")));
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// fldBrows
			// 
			this.fldBrows.Description = resources.GetString("fldBrows.Description");
			this.fldBrows.SelectedPath = resources.GetString("fldBrows.SelectedPath");
			this.fldBrows.ShowNewFolderButton = false;
			// 
			// txtPath
			// 
			this.txtPath.AccessibleDescription = resources.GetString("txtPath.AccessibleDescription");
			this.txtPath.AccessibleName = resources.GetString("txtPath.AccessibleName");
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtPath.Anchor")));
			this.txtPath.AutoSize = ((bool)(resources.GetObject("txtPath.AutoSize")));
			this.txtPath.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPath.BackgroundImage")));
			this.txtPath.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtPath.Dock")));
			this.txtPath.Enabled = ((bool)(resources.GetObject("txtPath.Enabled")));
			this.txtPath.Font = ((System.Drawing.Font)(resources.GetObject("txtPath.Font")));
			this.txtPath.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtPath.ImeMode")));
			this.txtPath.Location = ((System.Drawing.Point)(resources.GetObject("txtPath.Location")));
			this.txtPath.MaxLength = ((int)(resources.GetObject("txtPath.MaxLength")));
			this.txtPath.Multiline = ((bool)(resources.GetObject("txtPath.Multiline")));
			this.txtPath.Name = "txtPath";
			this.txtPath.PasswordChar = ((char)(resources.GetObject("txtPath.PasswordChar")));
			this.txtPath.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtPath.RightToLeft")));
			this.txtPath.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtPath.ScrollBars")));
			this.txtPath.Size = ((System.Drawing.Size)(resources.GetObject("txtPath.Size")));
			this.txtPath.TabIndex = ((int)(resources.GetObject("txtPath.TabIndex")));
			this.txtPath.Text = resources.GetString("txtPath.Text");
			this.txtPath.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtPath.TextAlign")));
			this.txtPath.Visible = ((bool)(resources.GetObject("txtPath.Visible")));
			this.txtPath.WordWrap = ((bool)(resources.GetObject("txtPath.WordWrap")));
			this.txtPath.TextChanged += new System.EventHandler(this.txtPath_TextChanged);
			// 
			// label3
			// 
			this.label3.AccessibleDescription = resources.GetString("label3.AccessibleDescription");
			this.label3.AccessibleName = resources.GetString("label3.AccessibleName");
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label3.Anchor")));
			this.label3.AutoSize = ((bool)(resources.GetObject("label3.AutoSize")));
			this.label3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label3.Dock")));
			this.label3.Enabled = ((bool)(resources.GetObject("label3.Enabled")));
			this.label3.Font = ((System.Drawing.Font)(resources.GetObject("label3.Font")));
			this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
			this.label3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.ImageAlign")));
			this.label3.ImageIndex = ((int)(resources.GetObject("label3.ImageIndex")));
			this.label3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label3.ImeMode")));
			this.label3.Location = ((System.Drawing.Point)(resources.GetObject("label3.Location")));
			this.label3.Name = "label3";
			this.label3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label3.RightToLeft")));
			this.label3.Size = ((System.Drawing.Size)(resources.GetObject("label3.Size")));
			this.label3.TabIndex = ((int)(resources.GetObject("label3.TabIndex")));
			this.label3.Text = resources.GetString("label3.Text");
			this.label3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.TextAlign")));
			this.label3.Visible = ((bool)(resources.GetObject("label3.Visible")));
			// 
			// btnBrowse
			// 
			this.btnBrowse.AccessibleDescription = resources.GetString("btnBrowse.AccessibleDescription");
			this.btnBrowse.AccessibleName = resources.GetString("btnBrowse.AccessibleName");
			this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnBrowse.Anchor")));
			this.btnBrowse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBrowse.BackgroundImage")));
			this.btnBrowse.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnBrowse.Dock")));
			this.btnBrowse.Enabled = ((bool)(resources.GetObject("btnBrowse.Enabled")));
			this.btnBrowse.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnBrowse.FlatStyle")));
			this.btnBrowse.Font = ((System.Drawing.Font)(resources.GetObject("btnBrowse.Font")));
			this.btnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowse.Image")));
			this.btnBrowse.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnBrowse.ImageAlign")));
			this.btnBrowse.ImageIndex = ((int)(resources.GetObject("btnBrowse.ImageIndex")));
			this.btnBrowse.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnBrowse.ImeMode")));
			this.btnBrowse.Location = ((System.Drawing.Point)(resources.GetObject("btnBrowse.Location")));
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnBrowse.RightToLeft")));
			this.btnBrowse.Size = ((System.Drawing.Size)(resources.GetObject("btnBrowse.Size")));
			this.btnBrowse.TabIndex = ((int)(resources.GetObject("btnBrowse.TabIndex")));
			this.btnBrowse.Text = resources.GetString("btnBrowse.Text");
			this.btnBrowse.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnBrowse.TextAlign")));
			this.btnBrowse.Visible = ((bool)(resources.GetObject("btnBrowse.Visible")));
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// label4
			// 
			this.label4.AccessibleDescription = resources.GetString("label4.AccessibleDescription");
			this.label4.AccessibleName = resources.GetString("label4.AccessibleName");
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label4.Anchor")));
			this.label4.AutoSize = ((bool)(resources.GetObject("label4.AutoSize")));
			this.label4.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label4.Dock")));
			this.label4.Enabled = ((bool)(resources.GetObject("label4.Enabled")));
			this.label4.Font = ((System.Drawing.Font)(resources.GetObject("label4.Font")));
			this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
			this.label4.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label4.ImageAlign")));
			this.label4.ImageIndex = ((int)(resources.GetObject("label4.ImageIndex")));
			this.label4.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label4.ImeMode")));
			this.label4.Location = ((System.Drawing.Point)(resources.GetObject("label4.Location")));
			this.label4.Name = "label4";
			this.label4.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label4.RightToLeft")));
			this.label4.Size = ((System.Drawing.Size)(resources.GetObject("label4.Size")));
			this.label4.TabIndex = ((int)(resources.GetObject("label4.TabIndex")));
			this.label4.Text = resources.GetString("label4.Text");
			this.label4.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label4.TextAlign")));
			this.label4.Visible = ((bool)(resources.GetObject("label4.Visible")));
			// 
			// radLat
			// 
			this.radLat.AccessibleDescription = resources.GetString("radLat.AccessibleDescription");
			this.radLat.AccessibleName = resources.GetString("radLat.AccessibleName");
			this.radLat.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("radLat.Anchor")));
			this.radLat.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("radLat.Appearance")));
			this.radLat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radLat.BackgroundImage")));
			this.radLat.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radLat.CheckAlign")));
			this.radLat.Checked = true;
			this.radLat.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("radLat.Dock")));
			this.radLat.Enabled = ((bool)(resources.GetObject("radLat.Enabled")));
			this.radLat.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("radLat.FlatStyle")));
			this.radLat.Font = ((System.Drawing.Font)(resources.GetObject("radLat.Font")));
			this.radLat.Image = ((System.Drawing.Image)(resources.GetObject("radLat.Image")));
			this.radLat.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radLat.ImageAlign")));
			this.radLat.ImageIndex = ((int)(resources.GetObject("radLat.ImageIndex")));
			this.radLat.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("radLat.ImeMode")));
			this.radLat.Location = ((System.Drawing.Point)(resources.GetObject("radLat.Location")));
			this.radLat.Name = "radLat";
			this.radLat.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("radLat.RightToLeft")));
			this.radLat.Size = ((System.Drawing.Size)(resources.GetObject("radLat.Size")));
			this.radLat.TabIndex = ((int)(resources.GetObject("radLat.TabIndex")));
			this.radLat.TabStop = true;
			this.radLat.Text = resources.GetString("radLat.Text");
			this.radLat.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radLat.TextAlign")));
			this.radLat.Visible = ((bool)(resources.GetObject("radLat.Visible")));
			// 
			// radNotLat
			// 
			this.radNotLat.AccessibleDescription = resources.GetString("radNotLat.AccessibleDescription");
			this.radNotLat.AccessibleName = resources.GetString("radNotLat.AccessibleName");
			this.radNotLat.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("radNotLat.Anchor")));
			this.radNotLat.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("radNotLat.Appearance")));
			this.radNotLat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radNotLat.BackgroundImage")));
			this.radNotLat.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radNotLat.CheckAlign")));
			this.radNotLat.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("radNotLat.Dock")));
			this.radNotLat.Enabled = ((bool)(resources.GetObject("radNotLat.Enabled")));
			this.radNotLat.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("radNotLat.FlatStyle")));
			this.radNotLat.Font = ((System.Drawing.Font)(resources.GetObject("radNotLat.Font")));
			this.radNotLat.Image = ((System.Drawing.Image)(resources.GetObject("radNotLat.Image")));
			this.radNotLat.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radNotLat.ImageAlign")));
			this.radNotLat.ImageIndex = ((int)(resources.GetObject("radNotLat.ImageIndex")));
			this.radNotLat.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("radNotLat.ImeMode")));
			this.radNotLat.Location = ((System.Drawing.Point)(resources.GetObject("radNotLat.Location")));
			this.radNotLat.Name = "radNotLat";
			this.radNotLat.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("radNotLat.RightToLeft")));
			this.radNotLat.Size = ((System.Drawing.Size)(resources.GetObject("radNotLat.Size")));
			this.radNotLat.TabIndex = ((int)(resources.GetObject("radNotLat.TabIndex")));
			this.radNotLat.Text = resources.GetString("radNotLat.Text");
			this.radNotLat.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("radNotLat.TextAlign")));
			this.radNotLat.Visible = ((bool)(resources.GetObject("radNotLat.Visible")));
			// 
			// groupBox1
			// 
			this.groupBox1.AccessibleDescription = resources.GetString("groupBox1.AccessibleDescription");
			this.groupBox1.AccessibleName = resources.GetString("groupBox1.AccessibleName");
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("groupBox1.Anchor")));
			this.groupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox1.BackgroundImage")));
			this.groupBox1.Controls.Add(this.cmbExt);
			this.groupBox1.Controls.Add(this.radLat);
			this.groupBox1.Controls.Add(this.radNotLat);
			this.groupBox1.Controls.Add(this.txtName);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("groupBox1.Dock")));
			this.groupBox1.Enabled = ((bool)(resources.GetObject("groupBox1.Enabled")));
			this.groupBox1.Font = ((System.Drawing.Font)(resources.GetObject("groupBox1.Font")));
			this.groupBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("groupBox1.ImeMode")));
			this.groupBox1.Location = ((System.Drawing.Point)(resources.GetObject("groupBox1.Location")));
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("groupBox1.RightToLeft")));
			this.groupBox1.Size = ((System.Drawing.Size)(resources.GetObject("groupBox1.Size")));
			this.groupBox1.TabIndex = ((int)(resources.GetObject("groupBox1.TabIndex")));
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = resources.GetString("groupBox1.Text");
			this.groupBox1.Visible = ((bool)(resources.GetObject("groupBox1.Visible")));
			// 
			// cmbExt
			// 
			this.cmbExt.AccessibleDescription = resources.GetString("cmbExt.AccessibleDescription");
			this.cmbExt.AccessibleName = resources.GetString("cmbExt.AccessibleName");
			this.cmbExt.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmbExt.Anchor")));
			this.cmbExt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmbExt.BackgroundImage")));
			this.cmbExt.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmbExt.Dock")));
			this.cmbExt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbExt.Enabled = ((bool)(resources.GetObject("cmbExt.Enabled")));
			this.cmbExt.Font = ((System.Drawing.Font)(resources.GetObject("cmbExt.Font")));
			this.cmbExt.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmbExt.ImeMode")));
			this.cmbExt.IntegralHeight = ((bool)(resources.GetObject("cmbExt.IntegralHeight")));
			this.cmbExt.ItemHeight = ((int)(resources.GetObject("cmbExt.ItemHeight")));
			this.cmbExt.Location = ((System.Drawing.Point)(resources.GetObject("cmbExt.Location")));
			this.cmbExt.MaxDropDownItems = ((int)(resources.GetObject("cmbExt.MaxDropDownItems")));
			this.cmbExt.MaxLength = ((int)(resources.GetObject("cmbExt.MaxLength")));
			this.cmbExt.Name = "cmbExt";
			this.cmbExt.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmbExt.RightToLeft")));
			this.cmbExt.Size = ((System.Drawing.Size)(resources.GetObject("cmbExt.Size")));
			this.cmbExt.TabIndex = ((int)(resources.GetObject("cmbExt.TabIndex")));
			this.cmbExt.Text = resources.GetString("cmbExt.Text");
			this.cmbExt.Visible = ((bool)(resources.GetObject("cmbExt.Visible")));
			this.cmbExt.SelectedIndexChanged += new System.EventHandler(this.cmbExt_SelectedIndexChanged);
			// 
			// txtName
			// 
			this.txtName.AccessibleDescription = resources.GetString("txtName.AccessibleDescription");
			this.txtName.AccessibleName = resources.GetString("txtName.AccessibleName");
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtName.Anchor")));
			this.txtName.AutoSize = ((bool)(resources.GetObject("txtName.AutoSize")));
			this.txtName.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtName.BackgroundImage")));
			this.txtName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtName.Dock")));
			this.txtName.Enabled = ((bool)(resources.GetObject("txtName.Enabled")));
			this.txtName.Font = ((System.Drawing.Font)(resources.GetObject("txtName.Font")));
			this.txtName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtName.ImeMode")));
			this.txtName.Location = ((System.Drawing.Point)(resources.GetObject("txtName.Location")));
			this.txtName.MaxLength = ((int)(resources.GetObject("txtName.MaxLength")));
			this.txtName.Multiline = ((bool)(resources.GetObject("txtName.Multiline")));
			this.txtName.Name = "txtName";
			this.txtName.PasswordChar = ((char)(resources.GetObject("txtName.PasswordChar")));
			this.txtName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtName.RightToLeft")));
			this.txtName.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtName.ScrollBars")));
			this.txtName.Size = ((System.Drawing.Size)(resources.GetObject("txtName.Size")));
			this.txtName.TabIndex = ((int)(resources.GetObject("txtName.TabIndex")));
			this.txtName.Text = resources.GetString("txtName.Text");
			this.txtName.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtName.TextAlign")));
			this.txtName.Visible = ((bool)(resources.GetObject("txtName.Visible")));
			this.txtName.WordWrap = ((bool)(resources.GetObject("txtName.WordWrap")));
			this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
			// 
			// label6
			// 
			this.label6.AccessibleDescription = resources.GetString("label6.AccessibleDescription");
			this.label6.AccessibleName = resources.GetString("label6.AccessibleName");
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label6.Anchor")));
			this.label6.AutoSize = ((bool)(resources.GetObject("label6.AutoSize")));
			this.label6.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label6.Dock")));
			this.label6.Enabled = ((bool)(resources.GetObject("label6.Enabled")));
			this.label6.Font = ((System.Drawing.Font)(resources.GetObject("label6.Font")));
			this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
			this.label6.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label6.ImageAlign")));
			this.label6.ImageIndex = ((int)(resources.GetObject("label6.ImageIndex")));
			this.label6.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label6.ImeMode")));
			this.label6.Location = ((System.Drawing.Point)(resources.GetObject("label6.Location")));
			this.label6.Name = "label6";
			this.label6.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label6.RightToLeft")));
			this.label6.Size = ((System.Drawing.Size)(resources.GetObject("label6.Size")));
			this.label6.TabIndex = ((int)(resources.GetObject("label6.TabIndex")));
			this.label6.Text = resources.GetString("label6.Text");
			this.label6.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label6.TextAlign")));
			this.label6.Visible = ((bool)(resources.GetObject("label6.Visible")));
			// 
			// label5
			// 
			this.label5.AccessibleDescription = resources.GetString("label5.AccessibleDescription");
			this.label5.AccessibleName = resources.GetString("label5.AccessibleName");
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label5.Anchor")));
			this.label5.AutoSize = ((bool)(resources.GetObject("label5.AutoSize")));
			this.label5.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label5.Dock")));
			this.label5.Enabled = ((bool)(resources.GetObject("label5.Enabled")));
			this.label5.Font = ((System.Drawing.Font)(resources.GetObject("label5.Font")));
			this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
			this.label5.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label5.ImageAlign")));
			this.label5.ImageIndex = ((int)(resources.GetObject("label5.ImageIndex")));
			this.label5.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label5.ImeMode")));
			this.label5.Location = ((System.Drawing.Point)(resources.GetObject("label5.Location")));
			this.label5.Name = "label5";
			this.label5.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label5.RightToLeft")));
			this.label5.Size = ((System.Drawing.Size)(resources.GetObject("label5.Size")));
			this.label5.TabIndex = ((int)(resources.GetObject("label5.TabIndex")));
			this.label5.Text = resources.GetString("label5.Text");
			this.label5.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label5.TextAlign")));
			this.label5.Visible = ((bool)(resources.GetObject("label5.Visible")));
			// 
			// txtDesc
			// 
			this.txtDesc.AccessibleDescription = resources.GetString("txtDesc.AccessibleDescription");
			this.txtDesc.AccessibleName = resources.GetString("txtDesc.AccessibleName");
			this.txtDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDesc.Anchor")));
			this.txtDesc.AutoSize = ((bool)(resources.GetObject("txtDesc.AutoSize")));
			this.txtDesc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDesc.BackgroundImage")));
			this.txtDesc.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDesc.Dock")));
			this.txtDesc.Enabled = ((bool)(resources.GetObject("txtDesc.Enabled")));
			this.txtDesc.Font = ((System.Drawing.Font)(resources.GetObject("txtDesc.Font")));
			this.txtDesc.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDesc.ImeMode")));
			this.txtDesc.Location = ((System.Drawing.Point)(resources.GetObject("txtDesc.Location")));
			this.txtDesc.MaxLength = ((int)(resources.GetObject("txtDesc.MaxLength")));
			this.txtDesc.Multiline = ((bool)(resources.GetObject("txtDesc.Multiline")));
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.PasswordChar = ((char)(resources.GetObject("txtDesc.PasswordChar")));
			this.txtDesc.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDesc.RightToLeft")));
			this.txtDesc.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDesc.ScrollBars")));
			this.txtDesc.Size = ((System.Drawing.Size)(resources.GetObject("txtDesc.Size")));
			this.txtDesc.TabIndex = ((int)(resources.GetObject("txtDesc.TabIndex")));
			this.txtDesc.Text = resources.GetString("txtDesc.Text");
			this.txtDesc.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDesc.TextAlign")));
			this.txtDesc.Visible = ((bool)(resources.GetObject("txtDesc.Visible")));
			this.txtDesc.WordWrap = ((bool)(resources.GetObject("txtDesc.WordWrap")));
			// 
			// AddBibleForm
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.ControlBox = false;
			this.Controls.Add(this.txtDesc);
			this.Controls.Add(this.txtPath);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.panel1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "AddBibleForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.AddBibleForm_Load);
			this.panel1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void AddBibleForm_Load(object sender, System.EventArgs e)
		{
			try {
			txtPath.Text=Startup.DataPath+"\\Text";
		} catch {}
		}

		private void txtPath_TextChanged(object sender, System.EventArgs e)
		{
			try {
			NextEnabled();

			cmbExt.Items.Clear();
			try
			{
				// try to guess the extension
				DirectoryInfo di=new DirectoryInfo( txtPath.Text );
				FileInfo[] fi;
				fi=di.GetFiles("Gn.*");
				foreach (FileInfo f in fi)
				{
					cmbExt.Items.Add(f.Extension);
					bool a=false;
					foreach (object b in Startup.Bibles)
					{
						if ("."+(b as Bible).Extension==f.Extension)
						{
							a=true;
							break;
						}
					}
					if (a==false)
					{
						cmbExt.SelectedIndex=cmbExt.Items.Count-1;
					}
				}
			}
			catch
			{
			}
		} catch {}
		}

		private void NextEnabled()
		{
			try {
			btnNext.Enabled=(txtName.Text.Length>0 && txtPath.Text.Length>0 && cmbExt.SelectedIndex>=0);
		} catch {}
		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			try {
			fldBrows.SelectedPath=txtPath.Text;
			fldBrows.ShowDialog();
			txtPath.Text=fldBrows.SelectedPath;
		} catch {}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			try {
			WasCancelled=true;
			this.Hide();
		} catch {}
		}

		private void btnNext_Click(object sender, System.EventArgs e)
		{
			try {
			bool a= false;
			foreach (object b in Startup.Bibles)
			{
				if (("."+(b as Bible).Extension)==((string) cmbExt.SelectedItem))
				{
					a=true;
					break;
				}
			}

			if (a)
			{
				if (txtPath.Text!=Startup.DataPath+"\\Text" )

				{
					DialogResult r=MessageBox.Show(Startup.rm.GetString("replacebible"),"VulSearch 4",MessageBoxButtons.YesNo);
					if (r==DialogResult.Yes) a=false;
				}
				else
				{
					a=false;
				}
			}

			if (!a)
			{
				if (Directory.Exists(txtPath.Text))
				{
					this.Hide();
				}
				else
				{
					MessageBox.Show(Startup.rm.GetString("pathnotfound"),"VulSearch 4",MessageBoxButtons.OK);
				}
			}
		} catch {}
		}

		private void txtName_TextChanged(object sender, System.EventArgs e)
		{
			try {
			NextEnabled();		
		} catch {}
		}

		private void cmbExt_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try {
			NextEnabled();

			// if it's a Bible we know about, fill in details
			string ex=(string) cmbExt.SelectedItem;
			if (ex.StartsWith(".")) ex=ex.Substring(1);
			int i;
			for (i=0; i<Startup.Bibles.Count; i++)
			{
				if (((Startup.Bibles[i]) as Bible).Extension==ex)
				{
					txtName.Text=((Startup.Bibles[i]) as Bible).Name;
					radLat.Checked=((Startup.Bibles[i]) as Bible).IsLatin ;
					radNotLat.Checked=!((Startup.Bibles[i]) as Bible).IsLatin ;
					txtDesc.Text=((Startup.Bibles[i]) as Bible).Desc;
					i=-1;
					break;
				}
			}

			if (i!=-1)
			{
				// try to guess whether it's Latin
				try
				{
					string ext=(string) cmbExt.SelectedItem;
					if (ext.StartsWith(".")) ext=ext.Substring(1);
					StreamReader reader;
					FileStream file = new FileStream(txtPath.Text+"\\Gn." + ext,
						FileMode.Open,FileAccess.Read);
					reader = new StreamReader(file, 
						System.Text.Encoding.GetEncoding(1252));
					if (reader.ReadLine().IndexOf("terram")==-1)
					{
						radNotLat.Checked=true;
					}
					else
					{
						radLat.Checked=true;
					}
					reader.Close();
				}
				catch
				{
				}
			}
		} catch {}
		}
	}
}
