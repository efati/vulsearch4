﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace VulSearch4
{
    public partial class TroubleForm : Form
    {
        public TroubleForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try{
                       
            if (MessageBox.Show("Are you sure?", "Run Troubleshooting Option 1", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                return;
            } catch {
                return;
            }

            try
            {
                Startup.troubleshootingModeDontWriteSettings = true;
                RegistryKey key = Registry.CurrentUser.OpenSubKey(
                "Software\\VulSearch4\\VulSearch4.2", true);
                key.DeleteValue("MainNewTop", false);
                key.DeleteValue("MainNewLeft", false);
                key.DeleteValue("MainNewHeight", false);
                key.DeleteValue("MainNewWidth", false);

                key.DeleteValue("BookmarksTop", false);
                key.DeleteValue("BookmarksLeft", false);
                key.DeleteValue("BookmarksHeight", false);
                key.DeleteValue("BookmarksWidth", false);
                key.DeleteValue("BookmarksVisible", false);

                key.DeleteValue("SearchTop", false);
                key.DeleteValue("SearchLeft", false);
                key.DeleteValue("SearchHeight", false);
                key.DeleteValue("SearchWidth", false);
                key.DeleteValue("SearchVisible", false);

                key.DeleteValue("RefsTop", false);
                key.DeleteValue("RefsLeft", false);
                key.DeleteValue("RefsHeight", false);
                key.DeleteValue("RefsWidth", false);
                key.DeleteValue("RefsVisible", false);

                key.DeleteValue("HistoryTop", false);
                key.DeleteValue("HistoryLeft", false);
                key.DeleteValue("HistoryHeight", false);
                key.DeleteValue("HistoryWidth", false);
                key.DeleteValue("HistoryVisible", false);

                key.DeleteValue("NotesTop", false);
                key.DeleteValue("NotesLeft", false);
                key.DeleteValue("NotesHeight", false);
                key.DeleteValue("NotesWidth", false);
                key.DeleteValue("NotesVisible", false);

                key.DeleteValue("ResultsTop", false);
                key.DeleteValue("ResultsLeft", false);
                key.DeleteValue("ResultsHeight", false);
                key.DeleteValue("ResultsWidth", false);
                key.DeleteValue("ResultsVisible", false);

                key.DeleteValue("WordsTop", false);
                key.DeleteValue("WordsLeft", false);
                key.DeleteValue("WordsHeight", false);
                key.DeleteValue("WordsWidth", false);
                key.DeleteValue("WordsVisible", false);

                key.DeleteValue("WindowsLocked", false);

                key.DeleteValue("SearchBMTop", false);
                key.DeleteValue("SearchBMLeft", false);
                key.DeleteValue("SearchBMHeight", false);
                key.DeleteValue("SearchBMWidth", false);
                key.DeleteValue("SearchBMCol0", false);
                key.Close();
                MessageBox.Show("Troubleshooter Option 1 ran successfully. VulSearch will now close: please restart.", "Troubleshooter", MessageBoxButtons.OK);
                Startup.MainForm.Close();
            }
            catch (Exception ex)
                {
                    MessageBox.Show("Troubleshooter Option 1 failed:\n" + ex.ToString() + "\nVulSearch will now close: please restart.", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Startup.MainForm.Close();
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Are you sure? You will lose all your VulSearch settings, but any bookmarks, cross references or notes you have created will not be changed.", "Run Troubleshooting Option 2", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                    return;
            }
            catch
            {
                return;
            }

            try
            {
                Startup.troubleshootingModeDontWriteSettings = true;
                RegistryKey key = Registry.CurrentUser.OpenSubKey(
                "Software\\VulSearch4", true);
                key.DeleteSubKeyTree("VulSearch4.2", false);
                key.Close();
                MessageBox.Show("Troubleshooter Option 2 ran successfully. VulSearch will now close: please restart.", "Troubleshooter", MessageBoxButtons.OK);
                Startup.MainForm.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Troubleshooter Option 2 failed:\n" + ex.ToString() + "\nVulSearch will now close: please restart.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Startup.MainForm.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Are you sure? You will lose all your VulSearch settings, and any bookmarks, cross references or notes you have created will be deleted (specifically, the folder " + Startup.DataPath + " will be deleted).", "Run Troubleshooting Option 3", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                    return;
            }
            catch
            {
                return;
            }

            try
            {
                Startup.troubleshootingModeDontWriteSettings = true;
                RegistryKey key = Registry.CurrentUser.OpenSubKey(
                "Software\\VulSearch4", true);
                key.DeleteSubKey("VulSearch4.2", false);
                key.Close();
                System.IO.Directory.Delete(Startup.DataPath, true);
                MessageBox.Show("Troubleshooter Option 3 ran successfully. VulSearch will now close: please restart.", "Troubleshooter", MessageBoxButtons.OK);
                Startup.MainForm.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Troubleshooter Option 3 failed:\n" + ex.ToString() + "\nVulSearch will now close: please restart.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Startup.MainForm.Close();
            }
        }
    }
}
